<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <base href="{{URL::asset('/')}}" target="_top">

    <link rel="stylesheet" href="{{{ URL::asset('css/bootstrap.min.css')}}}" />
    <link rel="stylesheet" href="{{{ URL::asset('css/font-awesome.min.css')}}}" />
    <link rel="stylesheet" href="{{{ URL::asset('css/custom.css')}}}" />
    <script type="text/javascript" src="{{{ URL::asset('js/jquery-3.1.1.min.js')}}}"></script>
    <script src="{{{ URL::asset('js/bootstrap.min.js')}}}"></script>
</head>
<body class="no-skin">

            @yield('content')

</body>
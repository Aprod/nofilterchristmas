@extends('layouts.app')


@section('content')
        <div class="container-fluid">
            <div class="parallax">
                <div class="header">
                    <div class="col-lg-6 col-lg-offset-3 col-sm-10 col-sm-offset-1">
                        <div class="logo hidden-xs hidden-sm">#NoFilterChristmas</div>
                    </div>
                    <div class="visible-xs visible-sm logo-xs">
                        <div class="no">#NoFilter</div>
                        <div class="christmas">Christmas</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="first-title text-center">What does Christmas means to you?</div>
            <div class="second-title text-center">Share it and inspire others!</div>
            <div class="christmas-feelings">
                <div class="title">This is the Christmas feelings box. Write your feelings and stories here!</div>
                <div class="box">
                    <textarea id="story" name="story" placeholder="Your story's place..." rows="8"></textarea>
                </div>
                <div class="ps">ps.: Don't forget to add #nofilterchristmas hashtag and the website's url: www.nofilterchristmas.com</div>
            </div>
            <div class="facebook-box">
                <div class="col-xs-12">
                    <div class="facebook login">
                        <div class="col-sm-5 text-center"><span class="step">Step 1.<span class="hidden-xs"> -> </span></span></div>
                        @if(Auth::check())
                            <div class="col-sm-5 "><a href="#" class="text done">Already logged in!</a></div>
                        @else
                            <div class="col-sm-5 "><a href="{!! url('/facebook/login'); !!}" class="text" id="login-button">Login with Facebook</a></div>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="facebook share">
                        <div class="col-sm-5 text-center"><span class="step">Step 2.<span class="hidden-xs"> -> </span></span></div>
                        <div class="col-sm-5 "><a id="share-button" class="text">Share to Facebook<i id="post-spinner" class="fa fa-circle-o-notch fa-spin hidden"></i>
                            </a></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="text-center">
                Contact Us: nofilterchristmas@gmail.com
            </div>
            <div class="text-center">
                <a href="privacy.html">Privacy policy</a>
                <a href="terms.html">Terms of usage</a>
            </div>
        </div>

    <script>
        $(function() {

            $("#story").val(localStorage.getItem('story'));

            $("#login-button").click(function(){
                localStorage.setItem('story', $("#story").val());
            });

            $("#share-button").click(function(){
                $("#post-spinner").removeClass('hidden');
                $.post( "facebook/post", { 'story': $("#story").val()}).done(function ( data ){
                    if (data.success){
                        $("#post-spinner").addClass('hidden');
                        $("#share-button").addClass('done');
                        $("#share-button").html('Already shared! Merry Christmas!')
                    }
                });
            });
        });
    </script>

@endsection
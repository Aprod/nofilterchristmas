<?php
/**
 * Created by PhpStorm.
 * User: aprodilles
 * Date: 2016. 11. 17.
 * Time: 23:01
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use SammyK\LaravelFacebookSdk\SyncableGraphNodeTrait;

class User extends Model
{
    use SyncableGraphNodeTrait;

    protected $table = 'user';

    protected static $graph_node_field_aliases = [
        'id' => 'facebook_user_id',
        'name' => 'full_name',
        'graph_node_field_name' => 'database_column_name',
    ];
}